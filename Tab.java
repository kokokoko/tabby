import org.rspeer.runetek.adapter.scene.Player;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.Game;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Dialog;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.component.tab.Skill;
import org.rspeer.runetek.api.component.tab.Skills;
import org.rspeer.runetek.api.input.Keyboard;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.runetek.event.listeners.ChatMessageListener;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.ChatMessageEvent;
import org.rspeer.runetek.event.types.ChatMessageType;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.Script;
import org.rspeer.script.ScriptCategory;
import org.rspeer.script.ScriptMeta;
import org.rspeer.ui.Log;

import java.awt.*;
import java.security.SecureRandom;

@ScriptMeta(name = "Tabby", desc = "Crafts Camelot teletabs in friend's houses", developer = "Koko", version = 2.0, category = ScriptCategory.MAGIC)
public class Tab extends Script implements RenderListener, ChatMessageListener {

    private int startXP, tabs = 0, startTabs = 0, lawRunes = 0, task = 0;
    private String status;
    private long startTime;

    @Override
    public void onStart() {
        startTime = System.currentTimeMillis();
        startXP = Skills.getExperience(Skill.MAGIC);
        startTabs = Inventory.getCount(true, "Camelot teleport");
        setTask(1);
        status = "Loading up!";
    }

    @Override
    public int loop() {
        Player Local = Players.getLocal();

        //Run
        if (!Movement.isRunEnabled())
            Movement.toggleRun(true);


        switch (getTask()) {
            case 1:
                //  SceneObject Scroll = SceneObjects.getNearest("Astronomical chart");//temp fix for slu1 house
                SceneObject Study = SceneObjects.getNearest(SceneObject -> SceneObject.getName().equals("Lectern") && SceneObject.containsAction("Study") /*&& Scroll.distance(SceneObject) < 10*/);
                if (Study != null) {
                    Log.info("Time to study teletabs");
                    status = "(" + getTask() + ") Processing studies";
                    Study.interact("Study");
                    Time.sleepUntil(() -> Interfaces.isOpen(79), 500, 10000);
                    Time.sleep(random(1000, 1500));
                    if (Interfaces.isOpen(79)) {
                        Log.info("Switching to phase 2");
                        setTask(2);
                    }
                } else {
                    //If inventory is full or if inventory has run out of noted soft clay--finish the job
                    if (Inventory.isFull() || (!Inventory.contains(1762) && Inventory.contains("Soft clay"))) {
                        Log.info("Inventory is full. Time to enter house");
                        status = "(" + getTask() + ") Entering friend's house";
                        SceneObject Portal = SceneObjects.getNearest(SceneObject -> SceneObject.getName().equals("Portal") && SceneObject.containsAction("Friend's house"));
                        if (Portal != null) {
                            Portal.interact("Friend's house");
                            Time.sleepUntil(() -> Interfaces.isVisible(162, 44), random(400, 500), 10000);
                            Time.sleep(random(250, 500));
                        }
                        if (Interfaces.isVisible(162, 44)) {
                            Log.info("Ready to press enter to get in the house!");
                            Keyboard.pressEnter();
                            Time.sleepUntil(() -> SceneObjects.getNearest("Lectern") != null, 1000, 10000);
                            Time.sleep(random(500, 1200));
                        }
                    } else {
                        status = "(" + getTask() + ") Un-noting clay";
                        if (Npcs.getNearest("Phials") != null) {
                            Log.info("Phials is here!");
                            if (Inventory.contains(1762)) {
                                if (!Inventory.isItemSelected() && !Dialog.isViewingChatOptions()) {
                                    Inventory.getFirst("Soft clay").interact("Use");
                                    Time.sleep(random(300, 700));
                                }
                                if (Inventory.isItemSelected() && !Dialog.isViewingChatOptions()) {
                                    Npcs.getNearest("Phials").interact("Use");
                                    Time.sleepUntil(() -> Dialog.isViewingChatOptions(), 1250, 10000);
                                }
                            }
                            if (Dialog.isViewingChatOptions()) {
                                Dialog.process(2);
                                Time.sleepUntil(() -> Inventory.isFull(), 1250, 10000);
                                Time.sleep(random(200, 500));
                            }
                            if (!Inventory.contains(1762) || !Inventory.contains(563)) {
                                Log.info("Out of materials, stopping script!");
                                setStopping(true);
                            }
                        }
                    }
                }
                break;
            case 2:
                status = "(" + getTask() + ") Studying tabs";
                if (Interfaces.isOpen(79)) {
                    Time.sleep(random(200, 300));
                    Interfaces.getComponent(79, 14).interact("Make-All"); //Camelot Teleport
                    Time.sleepUntil(() -> Local.isAnimating() || !Inventory.contains(1761), 1000, 10000);
                    if (Local.isAnimating()) {
                        setTask(3);
                        break;
                    } else if (!Inventory.contains(1761)) {
                        setTask(3);
                        break;
                    }
                }
                break;
            case 3:
                if (Inventory.contains(1761)) { //Un-noted Soft clay
                    status = "(" + getTask() + ") Tabby!";
                    Time.sleep(random(300, 1000));
                    if (Dialog.isViewingChat())
                        setTask(1);
                    if (Interfaces.isVisible(162, 44)) {
                        Log.info("Somehow this is still visible!");
                        Keyboard.pressEnter();
                        Time.sleep(random(500, 1200));
                    }
                } else {
                    int sleep = random(5000, 30000);
                    status = "(" + getTask() + ") Finished tabs. Sleeping for " + (sleep / 1000) + "s";
                    Time.sleep(sleep);
                    SceneObject Portal = SceneObjects.getNearest(SceneObject -> SceneObject.getName().equals("Portal") && SceneObject.containsAction("Lock") && SceneObject.containsAction("Enter"));
                    Portal.interact("Enter");
                    Time.sleepUntil(() -> Npcs.getNearest("Phials") != null, 1000, 10000);
                    if (Npcs.getNearest("Phials") != null) {
                        Time.sleep(random(500, 1000));
                        setTask(1);
                    }
                }
                break;
        }

        if (Inventory.contains("Law rune"))
            lawRunes = Inventory.getCount(true, "Law rune");
        if (Inventory.contains("Camelot teleport"))
            tabs = Inventory.getCount(true, "Camelot teleport");

        return random(200, 300);
    }

    @Override
    public void notify(ChatMessageEvent event) {
        if (event.getType().equals(ChatMessageType.TRADE_RECEIVED)) {
            if (event.getMessage().endsWith("privacy mode enabled.")) {
                Log.info("House is broken!");
                setStopping(true);
            }
        }
    }

    @Override
    public void notify(RenderEvent renderEvent) {
        double ETA = (lawRunes / getPerHour((tabs - startTabs))) * 60.0 * 1000.0;//60 minutes in an hour, 60 seconds in 1 minute, 1000ms in 1 second
        int nextLvlXp = Skills.getExperienceToNextLevel(Skill.MAGIC);//500
        int gainedXp = Skills.getExperience(Skill.MAGIC) - startXP;//75
        double ttl = (nextLvlXp / getPerHour(gainedXp)) * 60.0 * 1000.0;
        if (gainedXp == 0)
            ttl = 0;
        if ((tabs - startTabs) == 0)
            ETA = 0;
        Graphics g = renderEvent.getSource();
        g.setColor(Color.YELLOW);
        g.drawString("Status: " + status, 30, 285);
        g.drawString("Elapsed Time: " + formatTime(System.currentTimeMillis() - startTime) + " (ETA: " + formatTime(Double.valueOf(ETA).longValue()) + ")", 30, 300);
        g.drawString("Magic lvl: " + Skills.getCurrentLevel(Skill.MAGIC) + " (TTL: " + formatTime(Double.valueOf(ttl).longValue()) + ")", 30, 315);
        g.drawString("Tabs made: " + (tabs - startTabs) + " (" + String.format("%.2f", getPerHour((tabs - startTabs))) + "/hr)", 30, 330);
    }

    @Override
    public void onStop() {
        Log.info("Thanks for making tabs! Crafted " + (tabs - startTabs) + " in tabs this session.");
        Game.logout();
    }

    private int random(int min, int max) {
        SecureRandom random = new SecureRandom();
        return (random.nextInt(max - min + 1) + min);
    }

    private int getTask() {
        return task;
    }

    private void setTask(int id) {
        task = id;
    }

    private String formatTime(final long ms) {
        long s = ms / 1000, m = s / 60, h = m / 60;
        s %= 60;
        m %= 60;
        h %= 24;
        return String.format("%02d:%02d:%02d", h, m, s);
    }

    private double getPerHour(double value) {
        if ((System.currentTimeMillis() - startTime) > 0) {
            return value * 3600000d / (System.currentTimeMillis() - startTime);
        } else {
            return 0;
        }
    }
}